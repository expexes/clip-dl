# clip-dl

Downloading audio and video with [youtube-dl](https://rg3.github.io/youtube-dl) and clipboard.

depends:
- [dunstify](https://github.com/dunst-project/dunst)
- [xclip](https://www.archlinux.org/packages/extra/x86_64/xclip/)