#!/bin/bash

music_dir="~/Music"
default_dir="~/ClipDl"

url=$1
type=$2

if [[ -z $type ]]; then
	type="mp3"
fi

mkdir -p "$default_dir"

echo "... Downloading $url as $type"
if [[ "$type" = mp3 ]]; then
	youtube-dl -x --audio-format $type $url --exec "cp -a {} $music_dir ; rm {} ; dunstify --icon=\"clip-dl\" \"✅ Downloaded\" {}"
else
	youtube-dl $url -f $type --exec "cp -a {} $default_dir ; rm {} ; dunstify --icon=\"clip-dl\" \"✅ Downloaded\" {}"
fi
echo "  + End $url"